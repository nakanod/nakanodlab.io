# README

A blog powered by hugo.

## New post creation.

```console
$ hugo new post/YYYY/MM/new-post-slug.md
$ vi content/post/YYYY/MM/new-post.md
```

## Generate posts that include drafts and run local server.

```console
$ hugo -w -D server
```
