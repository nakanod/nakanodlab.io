+++
date = "2019-08-11T18:40:45+09:00"
#updated = "2019-08-11T18:40:45+09:00"
draft = false
markup = "md"
title = "btrfs なディスクのリサイズ"
slug = "resize-btrfs-disk"
toc = false
tags = ["btrfs"]
+++


GCP 上の btrfs フォーマットされた追加ディスクに対してオンラインリサイズを行います。
ディスクパーティションや btrfs のサブボリュームもありません。

最初に GCP のディスクを大きくします。
`DISK_NAME`, `PROJECT_ID`, `ZONE_NAME`, `DISK_SIZE` はそれぞれ環境に合わせて置き換えます。
`DISK_SIZE` はGB単位の数字で指定します。 (100GBにしたいなら 100 を指定)

```console
$ gcloud compute disks resize DISK_NAME --project PROJECT_ID --zone ZONE_NAME --size DISK_SIZE
```

ディスクを大きくしたら次はファイルシステムを拡張します。
対象のサーバー上で次のコマンドを実行します。 `MOUNT_POINT` は拡張したいファイルシステムのマウントポイントに置き換えます。 (例えば `/mnt` など)

```console
# btrfs filesystem resize max MOUNT_POINT
```

以上でオンラインリサイズは完了です。
