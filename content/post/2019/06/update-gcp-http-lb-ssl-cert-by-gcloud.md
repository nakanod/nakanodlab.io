+++
date = "2019-06-02T03:37:33+09:00"
#updated = "2019-06-02T03:37:33+09:00"
draft = false
markup = "md"
title = "gcloud コマンドで GCP HTTPS ロードバランサーの SSL 証明書を更新する"
slug = "update-gcp-http-lb-ssl-cert-by-gcloud"
toc = true
tags = ["gcp"]
+++

# 注意事項

2019年6月時点でマネージドのSSL証明書サービスがベータ版で存在していますので、
近いうちに後述する手順は必要ないものになりそうです。

[Creating and Using SSL Certificates  \|  Load Balancing  \|  Google Cloud](https://cloud.google.com/load-balancing/docs/ssl-certificates#managed-certs)


# 手順

gcloud の認証関連や証明書の取得はすでに済んでいる想定で進めます。
行うことは、以下の2つです。

1. SSL 証明書のアップロード
1. ロードバランサー(ターゲットプロキシ)に設定されている SSL 証明書を変更

## 1. SSL 証明書のアップロード

中間証明書が必要ならばそれも含めたファイルを `--certificate` オプションに指定します。

```console
$ gcloud compute ssl-certificates create <SSL_CERTIFICATE_NAME> --certificate=/path/to/cert/fullchain.pem --private-key=/path/to/key/privkey.pem
```

今アップロードされている証明書の確認や、不要なものを削除したいなら `list` や `delete` サブコマンドが使えます。

```console
$ gcloud compute ssl-certificates list
$ gcloud compute ssl-certificates delete <SSL_CERTIFICATE_NAME>
```

## 2. ロードバランサー(ターゲットプロキシ)に設定されている SSL 証明書を変更

以下のコマンドでターゲットプロキシに設定されている SSL 証明書を変更できます。

```console
$ gcloud compute target-https-proxies update <TARGET_PROXY_NAME> --ssl-certificates <SSL_CERTIFICATE_NAME>
```

SSL 証明書と同じく `list` サブコマンドでターゲットプロキシを確認できます。

```console
$ gcloud compute target-https-proxies list
```

以上で完了です。新しい証明書に切り替わるまで自身の体感として数分程度かかります。
少し待ってから変更されているか確認しましょう。
