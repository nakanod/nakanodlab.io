+++
date = "2019-03-16T17:25:43+09:00"
#updated = "2019-03-16T17:07:43+09:00"
draft = false
markup = "md"
title = "Ubuntu でのパッケージダウングレード"
slug = "downgrade-package-on-ubuntu"
toc = false
tags = ["ubuntu"]
+++

環境: Ubuntu 18.04

ちょっとした検証のためにアップグレードした Erlang 関連パッケージをいったん戻したいと思い、どのようにやるのか調べてみた。

`/etc/apt/preferences.d` 以下にファイルを作成して `apt upgrade` コマンドを実行すればよいようだ。

ファイルの作成

/etc/apt/preferences.d/erlang:

```text
Package: erlang-tools
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-inets
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-ssl
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-public-key
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-crypto
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-runtime-tools
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-mnesia
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-xmerl
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-dev
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-asn1
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-syntax-tools
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-parsetools
Pin: version 1:21.2.6-1
Pin-Priority: 1000

Package: erlang-base
Pin: version 1:21.2.6-1
Pin-Priority: 1000
```


コマンドの実行。

```console
$ sudo apt upgrade
```

これで無事ダウングレードされた。


参考

* [apt\_preferences\(5\) — apt — Debian stretch — Debian Manpages](https://manpages.debian.org/stretch/apt/apt_preferences.5.en.html)
* [apt\-getでパッケージをダウングレードする方法 \- Qiita](https://qiita.com/kenjiichige/items/0b8f94e3d425f6a5ce5a)
* [Debian\(Ubuntu\)で apt\-get upgrade で自動更新したくない場合の対応 \| レンタルサーバー・自宅サーバー設定・構築のヒント](https://server-setting.info/debian/debian-no-apt-upgrade.html)
