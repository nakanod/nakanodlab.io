+++
date = "2020-08-16T21:45:59+09:00"
#updated = "2020-08-16T18:48:59+09:00"
draft = false
markup = "md"
title = "Hugo のファイル生成時の警告対応"
slug = "fix-hugo-deprecated-warning"
toc = false
tags = ["hugo"]
+++

`hugo` コマンドで生成する際に以下の WARN ログが出ていた。

```text
Page.Hugo is deprecated and will be removed in a future release. Use the global hugo function.
```

`.Hugo` の書き方は将来削除されるので置き換えるのが良いようだ。確認したらテーマ内に `.Hugo.Generator` の記述があったので変更した。

```diff
-{{ .Hugo.Generator }}
+{{ hugo.Generator }}
```

1年ぶりくらいにHugoを使ったのでなにかしらの非推奨なものは出てくるだろうとは思ったけどさほど多くなくてよかった。

参考:

* [Hugo 0\.53→0\.55\.6に上げたときの警告の対応方法 \- Qiita](https://qiita.com/ikemo/items/5ad35021da46db47bb34)
* [Hugo\-specific Variables \| Hugo](https://gohugo.io/variables/hugo/)
