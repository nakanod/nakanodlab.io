+++
date = "2020-08-16T10:28:57+09:00"
#updated = "2020-08-16T01:47:57+09:00"
draft = false
markup = "md"
title = "Hugo で作成したブログを GitLab Pages にセットアップした"
slug = "setup-gitlab-pages"
toc = true
tags = ["gitlab"]
+++

もともと Hugo で生成した静的ファイルを GCLB backend bucket でホストしていたが、
お金がかかるので GitLab Pages に移動した。

リポジトリは以下に公開してある。

https://gitlab.com/nakanod/nakanod.gitlab.io  
https://gitlab.com/nakanod/hugo-theme-min

## テーマを `git submodule` で追加する

テーマを別リポジトリで管理していたものの、サブモジュールに追加していなかったので追加する。


```bash
git clone git@gitlab.com:nakanod/nakanod.gitlab.io.git
cd nakanod.gitlab.io/
git submodule add ../hugo-theme-min.git themes/min
git add .gitmodules
git commit -m 'Add theme via submodule'
```

## .gitlab-ci.yml の作成

GitLab Pages は CI を通して公開されるので `.gitlab-ci.yml` を作成して、 Git に追加する。

.gitlab-ci.yml:
```yaml
image: registry.gitlab.com/pages/hugo:latest

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  script:
  - hugo
  except:
  - master

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```

## Pages の公開設定

Gitlab プロジェクトの Settings > General > Visibility, project features, permissions > Pages を Everyone にする

## カスタムドメイン設定

1. プロジェクトの Settings > Pages > New Domain > Domain にカスタムドメインを入力する。
2. ドメイン所有確認の TXT レコードと、 ドメインをPagesに向けるための CNAME レコードをそれぞれ設定する。


