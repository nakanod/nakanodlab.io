+++
date = "2020-12-11T19:20:34+09:00"
#updated = "2020-12-11T18:45:34+09:00"
draft = false
markup = "md"
title = "CentOS 8 と Stream のパッケージバージョンの違い"
slug = "difference-centos-package-versions-between-8-and-stream"
toc = false
tags = ["centos"]
+++


AWS 東京リージョンで CentOS 8 を起動して試した。
2020/12/11時点でのバージョン情報。

EC2 AMI: ami-0d9bf167cb68ac889 (CentOS 8.3.2011)


2台起動して片方を Stream にコンバートする。

```text
# dnf install centos-release-stream
# dnf swap centos-{linux,stream}-repos
# dnf distro-sync
```

それぞれでパッケージをアップデート後、一覧表示を実行

```text
# yum update -y
$ rpm -qa | sort > /tmp/packages.txt
```

それぞれのパッケージ一覧を diff する
(長くなったのでGistに貼る。以下のリンク)

[centos\-8\-and\-stream\-packages\.diff](https://gist.github.com/nakanod/89afe4ed05151ec6daadadddcb3bdf8e)


半年後や1年後にもう一度同じように実行したらもっとバージョンに差が出るのだろうか。気になる。



参考:

* [CentOS Stream](https://www.centos.org/centos-stream/)
* [Cloud/AWS \- CentOS Wiki](https://wiki.centos.org/Cloud/AWS)

