+++
slug = "firewalld-allow-extra-port"
title = "firewalld で services の定義されていないポートの開放"
tags = [
  "firewalld",
]
date = "2017-03-05T00:37:18+09:00"
draft = false
markup = "md"

+++

独自に作成したデーモンプロセスを起動してポートを待ち受けている場合など add-service オプションが使用できない場面では
`--add-port=NUMBER/PROTO` のように指定して開放できる。 rich-rule での開放方法は `port port=NUMBER protocol=PROTO`
とすることで行える。 NUMBER はポート番号、 PROTO はプロトコル (tcp|udp) に置き換えること。

```console
# firewall-cmd --parmanent --zone=public --add-port=12345/tcp
# firewall-cmd --parmanent --zone=public --add-rich-rule='rule family=ipv4 source address=192.0.2.0/24 port port=12345 protocol=tcp accept'
```
