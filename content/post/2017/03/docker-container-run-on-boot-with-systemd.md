+++
title = "systemd で OS 起動時に Docker コンテナを立ち上げる"
slug = "docker-container-run-on-boot-with-systemd"
tags = [
  "docker", "systemd",
]
date = "2017-03-09T22:52:34+09:00"
draft = false
markup = "md"

+++

CoreOS を使っていると時折 Update Strategy のために OS を再起動してくる。
そんな時、 Docker のコンテナが OS 起動時に立ち上がらないと困ってしまう。
ファイル名は何でもいいが、 /etc/systemd/system 以下にファイルを作成して対処する。

```console
# vi /etc/systemd/system/web-server-container.service
```

/etc/systemd/system/web-server-container.service の内容は以下のようにする。
ExecStartPre で pull ExecStopPost で rm を行なっているのでレジストリにプッシュしておけば restart するだけで最新版にデプロイすることができる。

```ini
[Unit]
Description=My Docker Web Server Container
After=docker.service
Requires=docker.service

[Service]
ExecStartPre=/usr/bin/docker pull registry.example.com/web-server-container
ExecStart=/usr/bin/docker run -p 80:80 --name web-server-container -t registry.example.com/web-server-container
ExecStop=/usr/bin/docker stop -t 2 web-server-container
ExecStopPost=/usr/bin/docker rm -f web-server-container

[Install]
WantedBy=multi-user.target
```

OS 起動時に自動的に立ち上がるようにする。 (chkconfig 的なもの)
```console
# systemctl enable web-server-container.service
```

コンテナを立ち上げる。

```console
# systemctl start web-server-container.service
```
