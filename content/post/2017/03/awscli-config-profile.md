+++
slug = "awscli-config-profile"
title = "awscli config の profile"
tags = [
  "awscli",
]
date = "2017-03-14T23:33:44+09:00"
draft = false
markup = "md"

+++

awscli を使用するときに ~/.aws/credentials と ~/.aws/config とで profile の記述方法が違う。
以下の2つの aws configure コマンドを実行した時に出来上がる2つのファイルを見てみる。

```console
$ aws configure
$ aws configure --profile extra
```

~/.aws/credentials:

```ini
[default]
aws_access_key_id = XXXXXXXXXXXXXXXXXXXX
aws_secret_access_key = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

[extra]
aws_access_key_id = XXXXXXXXXXXXXXXXXXXX
aws_secret_access_key = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

~/.aws/config:

```ini
[default]
region = ap-northeast-1
output = json

[profile extra]
region = ap-northeast-1
output = json
```

~/.aws/config ファイルは `[profile extra]` という記述になる点に注意。

