+++
draft = false
markup = "md"
title = "Container Linux (CoreOS) のアップデートコマンド"
slug = "coreos-update-command"
tags = [
  "coreos",
]
date = "2017-03-20T19:40:52+09:00"

+++

Container Linux のアップデートは何かしらのタイミングで update\_engine というプロセスが走り、
その後、再起動して完了するというものだが、普段はバックグラウンドで自動的に行われる。

これを手動で行うには以下のコマンドを実行する。

```console
# update_engine_client -update
```
