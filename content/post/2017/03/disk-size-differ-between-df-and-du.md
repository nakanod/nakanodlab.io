+++
date = "2017-03-23T00:09:37+09:00"
draft = false
markup = "md"
slug = "disk-size-differ-between-df-and-du"
title = "df と du でディスクサイズが違うとき"
tags = [
  "misc",
]

+++

df と du で使用量に差があるときがある。
そんなときは以下のコマンドを使用するといい。

```console
# ls -al /proc/*/fd/* | grep deleted
```

ここで現れたプロセスをリスタートすれば差はなくなる。
