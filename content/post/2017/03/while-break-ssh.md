+++
slug = "while-break-ssh"
title = "while ループ内で SSH が1度しか実行されないのを回避する"
tags = [
  "openssh",
]
date = "2017-03-15T00:07:25+09:00"
draft = false
markup = "md"

+++

たとえば以下の内容のファイルを用意して、

hosts.txt:
```text
example.com
www.example.net
wiki.example.org
```

次のコマンドを実行してみた場合、

```console
$ while read line ; do ssh $line "hostname" ; done < hosts.txt
```

ここでやろうとしていることは hosts.txt に記述されたホストに順番に SSH していって hostname コマンドを実行するだけだが、
なぜか最初のホスト名を表示したタイミングで終了してしまう。これを回避するには ssh に -n オプションをつけて実行するか、
/dev/null 標準入力を渡してやると良い。

```console
$ while read line ; do ssh -n $line "hostname" ; done < hosts.txt
$ while read line ; do ssh $line "hostname" < /dev/null ; done < hosts.txt
```
