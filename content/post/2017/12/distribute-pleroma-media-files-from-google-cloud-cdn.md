+++
date = "2017-12-16T23:10:12+09:00"
#updated = "2017-12-16T22:18:12+09:00"
draft = false
markup = "md"
title = "Pleroma のメディアファイルを Google Cloud CDN から返すようにする"
slug = "distribute-pleroma-media-files-from-google-cloud-cdn"
toc = false
tags = ["pleroma", "gcp"]
+++

Pleroma を GCP HTTP Load Balancer の後ろへ配置して運用している人以外には無意味な内容。

ふとアイコン画像のレスポンスヘッダを見てみたら `Cache-Control: "public"` となっており、
これでは GCP HTTP Load Balancer で Cloud CDN を有効にしていたとしても機能しない。
(Cloud CDN 有効化には Cache-Control に public と max-age が必要)

Pleroma でメディアファイル(添付ファイルやアイコンなど)をアップロードすると、
`{PLEROMA_ROOT}/uploads` ディレクトリ内にファイルが保存され、それを Web からアクセスしようとしたら、
`https://pleroma.example.com/media/....` な感じの URL になる。

どうも、添付ファイルの場合は nginx が静的ファイルを返しているわけではなく、
Pleroma 側で返しているようだ。
で、ソースコード( `PLEROMA_ROOT}/lib/pleroma/web/endpoint.ex` の 12行目)に少し手を加えてやると目的は達成できた。
ちなみに検証時 Git のハッシュは `9223038319f0281f104fb9bb85c0cb8f6e52984a` (12/16時点 develop ブランチでの最新と思われる。)

lib/pleroma/web/endpoint.ex:

```diff
   # You should set gzip to true if you are running phoenix.digest
   # when deploying your static files in production.
   plug Plug.Static,
-    at: "/media", from: "uploads", gzip: false
+    at: "/media", from: "uploads", gzip: false, cache_control_for_etags: "public, max-age=31536000, immutable"
   plug Plug.Static,
     at: "/", from: :pleroma,
     only: ~w(index.html static finmoji emoji packs sounds sw.js)
```

その後、アプリケーションをビルドして、再起動しなおし、メディアファイルへアクセスしてみる。
レスポンスヘッダ内に Age ヘッダがあれば CDN は機能している。
