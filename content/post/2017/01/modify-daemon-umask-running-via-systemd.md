+++
date = "2017-01-31T00:08:00+09:00"
draft = false
markup = "md"
slug = "modify daemon umask running via systemd"
title = "systemd 経由で起動するデーモンの umask を変えたい"
tags = [
  "systemd", "umask",
]

+++

今度は umask を変えたい場合。だが、やることは ulimit とあまり変わらず systemctl edit foo.service して以下のような設定を加えれば良い。
これは umask 0002 に変えたい例。

```ini
[Service]
UMask=0002
```
