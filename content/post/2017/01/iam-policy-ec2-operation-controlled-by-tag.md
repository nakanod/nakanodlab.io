+++
tags = [
  "aws", "iam", "ec2",
]
date = "2017-01-22T02:04:24+09:00"
draft = false
markup = "md"
slug = "iam-policy-ec2-operation-controlled-by-tag"
title = "EC2 のタグにより操作を制御する IAM ポリシー"

+++

例えば、開発環境の EC2 インスタンスは起動・停止・再起動できてもいいよねといったことをしたい場合、
IAM のポリシーを下記の様に設定しておけばタグが `{"env": "dev"}` なインスタンスはそれらの操作を行うことができる。

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "ec2:Describe*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:StartInstances",
                "ec2:StopInstances",
                "ec2:RebootInstances"
            ],
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "ec2:ResourceTag/env": "dev"
                }
            }
        }
    ]
}
```
