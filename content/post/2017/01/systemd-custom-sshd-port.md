+++
tags = [
  "systemd",
]
date = "2017-01-22T14:33:16+09:00"
draft = false
markup = "md"
title = "systemd で SSHd のポート番号を変更する"
slug = "systemd-custom-sshd-port"

+++

CoreOS alpha 1284.2.0 で試した内容。
おそらく systemd を採用している他のディストリビューションでも同様にできると思うが未確認。
もしかしたら不可能かもしれないし、 sshd\_config を修正するやり方のほうが適切な場面もあるかも。
以下は22番から2222番に変更する例。

```console
# cp /usr/lib/systemd/system/sshd.socket /etc/systemd/system/sshd.socket
# vi /etc/systemd/system/sshd.socket
```

ListenStream の部分が編集箇所。

```diff
 [Socket]
-ListenStream=22
+ListenStream=2222
 Accept=yes
```
ちなみに ListenStream を複数行指定することも可能でその場合はそれぞれのポートを待ち受けることになる。

反映

```console
# systemctl daemon-reload
```

確認

```console
# systemctl status sshd.socket
```

実際に試してみたあと、 netstat で確認したらなぜか該当ポートを待ち受けてなかったので結局再起動した。
