+++
markup = "md"
slug = "systemd-custom-resource-limit"
title = "systemd 経由で起動するデーモンのリミット値を変更する"
tags = [
  "ulimit", "systemd",
]
date = "2017-01-29T18:46:03+09:00"
draft = false

+++

いわゆる ulimit 関連の話。ミドルウェアによってはディストリビューション標準の値で動作させると警告が出る場合がある。
systemctl edit コマンドを使用して対象のデーモンの設定を上書きする。
編集したファイルは /etc/systemd/system/{unit-file}.d/override.conf に保存される。
ソフトリミットとハードリミットを分けたい場合、コロン (:) で区切ることでそれぞれを soft:hard として設定できる。

```ini
[Service]
LimitNOFILE=65535:65535
LimitNPROC=65535:65535
```

リソース制限値を知りたい場合は ulimit コマンドを使用する。
ちなみに、 -a はすべて表示する。プロセスだけ知りたいなら -u を、オープンできるファイルを知りたいなら -n オプションを指定する。
-S はソフトリミット、 -H はハードリミットを表示する。

```console
$ ulimit -Sun  # プロセスとファイルのソフトリミットを表示
$ ulimit -Ha   # すべてのハードリミットを表示
```

起動中プロセスのリミットを確認したい場合は /proc/{pid}/limits を見れば良い。

```console
$ cat /proc/1/limits  # 例: init の limits を見る
```
