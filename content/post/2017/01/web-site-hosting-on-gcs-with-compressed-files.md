+++
markup = "md"
slug = "web-site-hosting-on-gcs-with-compressed-files"
title = "Google Cloud Storage でファイルを圧縮しての Web サイトホスティング"
tags = [
  "gcp", "gcs", "gsutil",
]
date = "2017-01-20T22:37:06+09:00"
draft = false

+++

html, css, js などの静的ファイルを、 Google Cloud Storage (GCS) へアップロードして公開する場面を想定する。

この手のオブジェクトストレージサービスは、
置いてあるデータのサイズが増えると課金額も増えるので、なるべく小さい状態を保つため圧縮して置いておきたい。
また、圧縮されたデータへのリクエスト時、ヘッダに `Accept-Encoding: gzip` がない場合は、自動的に解凍してレスポンスしてほしい。
GCS ではこのあたりをサポートしており以下2つの gsutil コマンドを実行して実現できる。

```console
$ gsutil -m cp -z txt,html,css,js -r src-dir/* gs://dest-bucket
$ gsutil -m setmeta -h 'Cache-Control: public, max-age=3600' -r gs://dest-bucket
```

まず1つ目の cp サブコマンドでは src-dir 下のファイルをアップロード、 -z オプションに圧縮したいファイルの拡張子を(複数あるならカンマ区切りで)指定する。 
-z オプションの拡張子を持つファイルは **ファイル名はそのまま** で gzip 圧縮されてアップロードされる。

次の setmeta サブコマンドではアップロードしたファイルのレスポンスヘッダ Cache-Control の設定を上書きしている。
これはなぜかというと -z オプション付きでアップロードされたファイルは `Cache-Control: no-transform` が自動的に付与されるようで、
これがあると `Accept-Encoding: gzip` ヘッダがないリクエストでも圧縮されたファイルをそのままレスポンスしてしまうため。

cp サブコマンド時に -h オプションで Cache-Control を指定することも可能だが、
no-transform が付与されてしまうところは回避できなかったため、 setmeta サブコマンドを別で実行することで対応した。
