+++
date = "2017-01-18T00:11:10+09:00"
draft = false
markup = "md"
slug = "curl-request-header"
title = "curl のリクエストヘッダを知りたい"
tags = [
  "curl",
]

+++

-v オプションを使用することで curl がどのようなリクエストをしているかを見ることができる。
とくに -H オプションを使用しない場合、 `Host`, `User-Agent`, `Accept` の3つのリクエストヘッダが最低限付与されるようだ。


```console
$ curl -v example.com/index.html
```
