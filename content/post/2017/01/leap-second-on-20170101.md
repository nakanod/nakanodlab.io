+++
draft = false
markup = "md"
slug = "leap-second-on-20170101"
title = "うるう秒"
tags = [
  "ntp",
]
date = "2017-01-05T22:42:39+09:00"

+++

日本時間2017年1月1日午前9時にうるう秒が挿入された。個人的にシステムのトラブルは特になかったが、この時にとった対応を今後のためにメモしておく。
今回は Google が公開している NTP サーバーを時刻の同期元にする、という対応をとった。
Google の NTP サーバーはうるう秒挿入時の前後10時間をかけて1秒を調整して、うるう秒を挿入せずにすませる。
そのため、このサーバーを同期元にしていれば、うるう秒が挿入されることはなくなる。

`ntp.conf` に以下のように設定を修正する。

```text
server time1.google.com iburst
server time2.google.com iburst
server time3.google.com iburst
server time4.google.com iburst
```

うるう秒が挿入されたサーバーでは syslog に以下のメッセージがあった。

```text
Clock: inserting leap second 23:59:60 UTC
```

Google の NTP サーバーを同期元としたサーバーには syslog にこのメッセージが出力されなかったので、うるう秒は挿入されなかったのだろうと思われる。
