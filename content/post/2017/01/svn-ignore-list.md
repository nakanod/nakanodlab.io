+++
date = "2017-01-18T23:20:11+09:00"
draft = false
markup = "md"
slug = "svn-ignore-list"
title = "Subversion の無視リスト"
tags = [
  "svn",
]

+++

Git が当たり前に使われるようになった時世であっても、やはり Subversion を使用するというタイミングはある。
Git でいうところの .gitignore ファイルのようにバージョン管理下に置きたくないファイルを設定するには propedit サブコマンドを使うことでおこなえる。

```console
$ cd /path/to/working/copy
$ svn propedit svn:ignore .
```

エディタが起動するので無視したいファイルやディレクトリを1行ずつ書いていく。
```text
*.pyc
*.pyo
tmp/
```

編集を終えたらコミットする。

```console
$ svn commit
```

propget サブコマンドで現在の状況を確認できる。

```console
$ svn propget svn:ignore .
```
