+++
date = "2017-01-18T23:53:22+09:00"
draft = false
markup = "md"
slug = "elb-enable-proxy-protocol"
tags = ["elb", "aws",]
title = "ELB Proxy Protocol を有効にする"

+++


```console
$ aws elb describe-load-balancer-policy-types  # 確認
$ aws elb create-load-balancer-policy --load-balancer-name my-loadbalancer --policy-name my-ProxyProtocol-policy --policy-type-name ProxyProtocolPolicyType --policy-attributes AttributeName=ProxyProtocol,AttributeValue=true
$ aws elb set-load-balancer-policies-for-backend-server --load-balancer-name my-loadbalancer --instance-port 80 --policy-names my-ProxyProtocol-policy my-existing-policy
$ aws elb describe-load-balancers --load-balancer-name my-loadbalancer
```
