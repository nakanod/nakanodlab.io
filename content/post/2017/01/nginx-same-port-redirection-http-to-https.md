+++
date = "2017-01-12T01:45:51+09:00"
draft = false
markup = "md"
slug = "nginx-same-port-redirection-http-to-https"
title = "nginx で http から同じ非標準ポートの https へリダイレクト"
tags = [
  "nginx",
]

+++

例えば、 http://example.com:8000/ を https://example.com:8000/ にリダイレクトしたい場面を考えてみる。
以下のような config があるとして、


```nginx
server {
    listen               8000 ssl;
    server_name          example.com;
    ssl_certificate      /path/to/cert;
    ssl_certificate_key  /path/to/key;
    # ...
}
```

ここでは8000番ポートへのアクセスは https であることを想定しており、 http でアクセスすると 400 Bad Request となる。
https でアクセスしなおせばいいのだが、どうせならリダイレクトしてほしい。そんな場合は以下の error_page ディレクティブを追加すると期待した動作が得られる。

```nginx
server {
    # ...
    error_page 497 https://$host:$server_port$request_uri;
    # ...
}
```
