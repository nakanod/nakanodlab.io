+++
draft = false
markup = "md"
slug = "curl-response-header"
title = "curl でレスポンスヘッダを表示する"
tags = [
  "curl",
]
date = "2017-01-18T00:05:21+09:00"

+++

-i オプションを使用すればレスポンスヘッダを表示できる。
-I オプションならヘッダだけを表示できるがリクエストメソッドが HEAD になる。 GET メソッドにしたいなら -X オプションと併用する。

```console
$ curl -i example.com/index.html
$ curl -I -X GET example.com/index.html
```
