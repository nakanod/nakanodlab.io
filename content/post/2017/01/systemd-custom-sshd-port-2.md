+++
tags = [
  "systemd",
]
date = "2017-01-22T23:01:40+09:00"
draft = false
markup = "md"
title = "systemd で SSHd のポート番号を変更する その2"
slug = "systemd-custom-sshd-port-2"

+++

この件についてもう少し調べてみるとどうも
/usr/lib/systemd/system 下のファイルを /etc/systemd/system へコピーする方法は、
少しスマートでは無さそうだ。というわけで別解。
一部分の設定を上書きする程度なら systemctl edit を使うほうが良い。

```console
# rm /etc/systemd/system/sshd.socket
# systemctl edit sshd.socket
```

systemctl edit コマンドを実行するとエディタが起動して /etc/systemd/system/{unit-file}.d/override.conf を編集できる。
以下の3行を加える。

```ini
[Socket]
ListenStream=
ListenStream=2222
```

上書きしたい設定のみを書けばいいので今回はこの3行だけで良い。
2行目に空な値を入れているのは、デフォルトの値を上書きしたいから。
この記述がないと、デフォルトに追加する動作になってしまい、22番と2222番を待ち受ける設定になる。
