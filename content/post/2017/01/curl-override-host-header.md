+++
tags = [
  "curl",
]
date = "2017-01-27T00:22:08+09:00"
draft = false
markup = "md"
title = "curl で Host ヘッダを指定すると便利"
slug = "curl-override-host-header"

+++

以下のようなコマンドを実行してみる。

```console
$ curl -H 'Host: example.com' http://192.0.2.1/index.html
```

192.0.2.1 のサーバー上にて Apache で名前ベースの VirtualHost 運用などをしていたとして、
これで example.com でアクセスしにきたものと同じレスポンスができる。
A レコードが設定されていないときにわざわざ /etc/hosts に書く手間が省ける。
また、なぜか名前解決ができないというような謎のネットワークから HTTP リクエストを送るのにも使える。(IP アドレスを覚えていればだが。)
