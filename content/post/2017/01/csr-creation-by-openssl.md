+++
draft = false
markup = "md"
slug = "csr-creation-by-openssl"
title = "OpenSSL による CSR 作成手順"
tags = [
  "openssl",
]
date = "2017-01-27T21:26:28+09:00"

+++

SSL 証明書の更新などのタイミングでいつも忘れているのでメモ。
CSR の作成には RSA 秘密鍵が必要になるので、まずはそれを作成しそのあと CSR を作成するという手順になる。

RSA 秘密鍵を作成する。

```console
$ openssl genrsa -rand /dev/urandom -out key.pem 2048
```

秘密鍵の確認は以下のコマンドで行える。

```console
$ openssl rsa -noout -text -in key.pem
```

CSR を作成する。 `-key` オプションに作成した秘密鍵を指定する。

```console
$ openssl req -new -key key.pem -out csr.pem -sha256
```
このコマンドで以下の9項目に入力を求められる。

```text
Country Name (2 letter code):
State or Province Name (full name):
Locality Name (eg, city):
Organization Name (eg, company):
Organizational Unit Name (eg, section):
Common Name (e.g. server FQDN or YOUR name):
Email Address:
A challenge password:
An optional company name:
```

以下にそれぞれの項目の意味を示しておく。自分の環境に合わせて適宜変更する。

| Text | 意味 | 例 |
| :-- | :-- | :-- |
| Country Name | 国名コード | JP |
| State or Province Name | 都道府県 | Tokyo |
| Locality Name | 市区町村 | Chiyoda |
| Organization Name | 組織名 | Example Company Inc |
| Organizational Unit Name | 部署名 | (たいていは空欄) |
| Common Name | FQDN | www.example.com や \*.example.com など |
| Email Address | メールアドレス | (たいていは空欄) |
| A challenge password | 証明書破棄時に必要になる? | (たいていは空欄) |
| An optional company name | 別の組織名 | (たいていは空欄) |

CSR の確認方法については以下のコマンドで行える。

```console
$ openssl req -noout -text -in csr.pem
```
