+++
tags = [
  "aws", "awscli", "cloudfront",
]
date = "2017-01-18T23:51:44+09:00"
draft = false
markup = "md"
slug = "cloudfront-create-invalidation-by-awscli"
title = "awscli で CloudFront のエッジキャッシュを削除する"

+++

2017/01時点で awscli での CloudFront 操作はプレビュー版とのことでデフォルトでは無効となっているのでまずは有効にする。

```console
$ aws configure set preview.cloudfront true
```
これで CloudFront を操作できるようになった。あとは以下のコマンドで Invalidation が作成され、
15〜20分ほどでエッジキャッシュが削除される。 `DISTRIBUTIONID` は適宜置き換える。

```console
$ aws cloudfront create-invalidation --distribution-id DISTRIBUTIONID --paths "/*"
```

Distribution や Invalidation を確認したい場合は以下のコマンドを使う。

```console
$ aws cloudfront list-distributions
$ aws cloudfront get-distribution --id DISTRIBUTIONID

$ aws cloudfront list-invalidations
$ aws cloudfront get-invalidation --distribution-id DISTRIBUTIONID --id INVALIDATIONID
```
