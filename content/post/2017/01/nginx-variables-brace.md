+++
date = "2017-01-08T23:07:04+09:00"
draft = false
markup = "md"
slug = "nginx-variables-brace"
title = "nginx の conf は変数を波括弧で囲める"
tags = [
  "nginx",
]

+++

Bash などと同様に変数を波括弧で囲める。ただし、セミコロンの直前に閉じ括弧が来ると Syntax Error になってしまうようで、それ以外の部分で使用する。もしくはダブルコーテーションで囲むなどして対処する。

```nginx
rewrite ^(.*)$ https://${host}$request_uri;       # ok
rewrite ^(.*)$ https://$host${request_uri};       # error
rewrite ^(.*)$ "https://$host${request_uri}";     # ok
```
