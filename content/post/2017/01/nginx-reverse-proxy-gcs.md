+++
tags = [
  "nginx",
]
date = "2017-01-29T00:07:31+09:00"
draft = false
markup = "md"
slug = "nginx-reverse-proxy-gcs"
title = "nginx で Google Cloud Storage をリバースプロキシ"

+++

Google Cloud Storage (GCS) 単独での Web サイトホスティングは、独自のドメインかつ HTTPS で行いたいとなると現時点では不可能。
そういった場面の解決方法の1つとして GCE で nginx のサーバーを立てリバースプロキシとしてやる方法がある。

```nginx
server {
    server_name www.example.com;
    # ...

    location / {
        proxy_set_header Host storage.googleapis.com;
        proxy_pass https://storage.googleapis.com/bucket-name$uri;
        # ...
    }
}
```

最低限、 proxy\_set\_header と proxy\_pass の2つがあれば良いが、実用にはプロキシキャッシュや SSL 関連の設定も必要。


ちなみに backend-buckets という Google HTTP Load Balancer と GCS を組み合わせられる機能がアルファ版で存在しており、
それを使用すればサーバーを立てる必要も無くなりそう。 もしくは Amazon の CloudFront + S3 を使う手もある。
