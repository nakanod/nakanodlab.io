+++
date = "2017-10-14T14:35:33+09:00"
#updated = "2017-10-14T14:35:33+09:00"
draft = false
markup = "md"
title = "パスワード管理ソフトウェアの不安"
slug = "password-manager"
toc = false
tags = ["misc"]
+++

私はパスワード管理ソフトウェアに KeePassX を使用していた。

先日 Gentoo をアップデートした際に app-admin/keepassx パッケージがマスクされ、
近いうちに Gentoo の公式パッケージからは削除されるので、
代替として KeePassXC の使用を提案するメッセージが表示された。

> Dead upstream. No qt5 release. Use app-admin/keepassxc as drop-in replacement which has very active upstream and regular releases.

現在は KeePassXC を使用している。
今まで使っていた .kdbx 形式のファイルはそのまま使うことができ、移行に関してまったく問題はなかったものの、
もし今使っているパスワード管理ソフトウェアがいきなり使えなくなったらどうすればと不安になった。

最近、 CLI のパスワード管理ツールを自身で作ったという投稿を何度か見かけた時に、なぜ作ったのかというところで、
おそらくは自身にとって使いやすいものがなかったという理由が主だと思うが、もしかしたらこういった不安もあったのかもしれない。
