+++
date = "2017-10-08T15:57:49+09:00"
draft = false
markup = "md"
slug = "upgrade-mastodon-db-from-9.6-to-10"
title = "Mastodon の DB を PostgreSQL 9.6から10へアップグレードした"
tags = ["mastodon", "postgresql", "docker"]
+++


先日 PostgreSQL 10がリリースされたので Mastodon の DB を9.6から10へアップグレードしました。
大まかな流れとしては現行の9.6と新しく10の環境を並行して用意しておき、9.6のダンプを取得し、それを10へリストアする形で移行しました。


## 環境

DB は redis と同居していますが、 App とは別のサーバで動いており、 基本的に docker-compose によって管理されています。

* GCE f1-micro 3台
    * Web (nginx)
    * App (rails, nodejs)
    * DB (postgres, redis) 
* Ubuntu 16.04
* docker-ce 17.09
* docker-compose 1.16.1
* postgres:9.6-alpine
* postgres:10.0-alpine

## 手順

1. App サーバを止めておき、 DB へのアクセスが来ない状況にしておきます。
1. DB のダンプを取得します。
1. 新旧環境を並行動作させるよう、 docker-compose.yml を編集、ボリューム用ディレクトリを作成します。
1. 新旧環境を起動します。
1. 新環境へ取得したダンプをリストアします。
1. 新環境へ切り替えます。


ダンプを取得

```console
$ docker exec mastodon_db_1 pg_dumpall -U postgres > /tmp/dump.sql
```

新環境用のボリュームディレクトリを作成する

```console
$ sudo mkdir /var/lib/mastodon/postgres10
```

新旧環境を同時に動かすため、 docker-compose.yml を編集する。新環境側は念の為、晒すポート番号を変更している。

```diff
   db:
     restart: always
     image: postgres:9.6-alpine
     ports:
       - "5432:5432"
     volumes:
       - /var/lib/mastodon/postgres:/var/lib/postgresql/data
+
+  dbnew:
+    restart: always
+    image: postgres:10.0-alpine
+    ports:
+      - "5433:5432"
+    volumes:
+      - /var/lib/mastodon/postgres10:/var/lib/postgresql/data
 
```

docker コンテナを再起動します。

```console
$ docker-compose down
$ docker-compose up -d
```

9.6のダンプを10へリストア

```console
$ cat /tmp/dump.sql | docker exec -i mastodon_dbnew_1 psql -U postgres
```

新旧環境切り替えのため一旦 docker コンテナを停止して、ボリューム用ディレクトリのリネーム

```console
$ docker-compose down

$ sudo mv /var/lib/mastodon/postgres /var/lib/mastodon/postgres9.6
$ sudo mv /var/lib/mastodon/postgres10 /var/lib/mastodon/postgres
```

新環境だけを起動するように docker-compose.yml を再編集

```diff
   db:
     restart: always
-    image: postgres:9.6-alpine
+    image: postgres:10.0-alpine
     ports:
       - "5432:5432"
     volumes:
       - /var/lib/mastodon/postgres:/var/lib/postgresql/data
-
-  dbnew:
-    restart: always
-    image: postgres:10.0-alpine
-    ports:
-      - "5433:5432"
-    volumes:
-      - /var/lib/mastodon/postgres10:/var/lib/postgresql/data
 
```

docker コンテナの起動

```console
$ docker-compose up -d
```

DB 作業は以上です。あとは App サーバを立ち上げて問題なくアクセスできることを確認します。

## 参考

* [Upgrading between major versions? · Issue \#37 · docker\-library/postgres · GitHub](https://github.com/docker-library/postgres/issues/37)
