+++
title = "firewalld でアクセス元 IP アドレスによる許可設定"
slug = "firewalld-access-controll-by-source-ip-address"
tags = [
  "firewalld",
]
date = "2017-02-21T20:34:12+09:00"
draft = false
markup = "md"

+++

以下のようにして add-rich-rule オプションを使えば アクセス元 IP アドレスによって制限することができる。

```console
# firewall-cmd --parmanent --zone=public --add-rich-rule='rule family=ipv4 source address=192.0.2.1 service name=ssh accept'
```

CIDR を使ってアクセス元ネットワーク範囲で指定することも可能。

```console
# firewall-cmd --parmanent --zone=public --add-rich-rule='rule family=ipv4 source address=192.0.2.0/24 service name=ssh accept'
```
