+++
markup = "md"
slug = "nginx-redirection-http-to-https-behind-load-balancer"
title = "Load Balancer 下にある nginx で https へのリダイレクト"
tags = [
  "nginx",
]
date = "2017-02-20T20:09:50+09:00"
draft = false

+++

Load Balancer 側で http と https を待ち受けており、
その下にある nginx が80番ポートしか待ち受けていない場面で http から https へリダイレクトしたい場合

```nginx
server {
    Listen 80;
    server_name www.example.com;
    # ...

    if ( $http_x_forwarded_proto = 'http' ) {
        return 301 https://$host$request_uri;
    }
}
```

ただしこれは Load Balancer が X-Forwarded-Proto を nginx に渡してくれるという前提がなければ機能しない。
そうでなければループしてしまうので注意。 
