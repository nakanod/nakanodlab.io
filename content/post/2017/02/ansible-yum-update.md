+++
slug = "ansible-yum-update"
title = "Ansible で yum update する"
tags = [
  "ansible",
]
date = "2017-02-22T22:11:35+09:00"
draft = false
markup = "md"

+++

まずはインベントリファイルを作成。

hosts\_sample:

```text
[all]
www.example.com
wiki.example.net
blog.example.org
```

次に以下のコマンドを実行する。ここでの login\_user は実際に SSH でログインするユーザーに置き換え、そして、リモートで sudo により root に昇格できる権限を持っていること。
パスワードなしで昇格できるなら -K オプションは必要なし。

```console
$ ansible -u login_user -K --become -i hosts_sample -m yum -a 'name=* state=latest' all
```

これでインベントリファイルに書かれた複数のホストに一斉に `yum update` をかけることができる。
