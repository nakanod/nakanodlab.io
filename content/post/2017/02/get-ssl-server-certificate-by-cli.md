+++
date = "2017-02-04T20:49:06+09:00"
draft = false
markup = "md"
slug = "get-ssl-server-certificate-by-cli"
title = "サイトのサーバー証明書をコマンドラインで確認する"
tags = [
  "openssl",
]

+++

SSL サーバー証明書を更新して、設定が反映されているかをサイトにアクセスして確認したい時の話。

ブラウザでアクセスして確認すれば良いじゃないかと思われるかもしれないが、コマンドラインでやりたい。
そんな時は openssl の s\_client サブコマンドを使用する。 -servername オプションを指定すれば SNI でも大丈夫。

```console
$ openssl s_client -servername ssl.example.com -connect ssl.example.com:443 < /dev/null | openssl x509 -text -noout
```

ちなみにこれは https のクライアントというわけではないので、ポート番号である443の部分を変更すれば
https 以外の SSL を使用したプロトコルに対してもサーバー証明書を確認できる。
