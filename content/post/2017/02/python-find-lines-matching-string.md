+++
title = "python でファイル内からある文字列に一致する行を抽出する"
slug = "python-find-lines-matching-string"
tags = [
  "python",
]
date = "2017-02-06T22:13:45+09:00"
draft = false
markup = "md"

+++

いわゆる grep 的なもの。ただし正規表現、ワイルドカードなものは無し。

```py
with open('/path/to/textfile', mode='r') as f:
    lines = f.readlines()

matchlines = [i for i in lines if i.find('search-string') >= 0]
```
