+++
date = "2017-02-26T21:25:26+09:00"
draft = false
markup = "md"
title = "firewalld 独自のポートを service で管理する"
slug = "firewalld-add-custom-port-to-service"
tags = [
  "firewalld",
]

+++

独自ポートを add-port オプションなどで開放することができるけれど、
http のような一般的なプロトコルと同じように add-service オプションを使用して開放させるようにしたい。

たとえばTCPで12345番ポートを待ち受ける foo-service を作成したいときは /etc/firewalld/services に以下のような XML を新規作成する。

/etc/firewalld/services/foo-service.xml:

```xml
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>Foo service</short>
  <description>Foo service description.</description>
  <port protocol="tcp" port="12345"/>
</service>
```

これであとは firewall-cmd を実行すれば良い。

```console
# firewall-cmd --parmanent --zone=public --add-service=foo-service
```
