+++
date = "2017-02-16T21:58:02+09:00"
draft = false
markup = "md"
title = "ELB 下の EC2 インスタンスを付けたり外したりする IAM ポリシー"
slug = "iam-policy-elb-register-or-deregister-instance"
tags = [
  "aws", "iam",
]

+++

以下のようなポリシーであれば、東京リージョンにある名前が my-loadbalancer のロードバランサーに
EC2 インスタンスを付けたり外したりできる。 1234567890 は AWS アカウント ID に置き換えること。

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
                "elasticloadbalancing:DeregisterInstancesFromLoadBalancer"
            ],
            "Resource": "arn:aws:elasticloadbalancing:ap-northeast-1:1234567890:loadbalancer/my-loadbalancer"
        }
    ]
}
```
