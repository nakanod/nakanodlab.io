+++
tags = [
  "letsencrypt", "dehydrated",
]
date = "2017-02-24T23:39:32+09:00"
draft = false
markup = "md"
title = "dehydrated を使って SSL 証明書を発行してみた"
slug = "ssl-certificate-creation-by-dehydrated"

+++

dehydrated は Let's Encrypt のクライアント(?)である。

普通に Let's Encrypt するなら certbot を使って http-01 あたりの方式で取得するのだろうけど、
これには Web サーバーが必要で、 A レコードのあたってるサーバーから実行しなくてはならないみたいなとこもあり、
若干使いづらさがある。

そこで dns-01 方式を行える dehydrated を試してみた。
(certbot でも dns-01 での取得が行えるみたいだが未確認)

```console
$ git clone https://github.com/lukas2511/dehydrated.git dehydrated
$ cd dehydrated
$ vi config domains.txt hooks.sh
$ chmod +x hooks.sh
```

ここで 設定ファイル、ドメインを記述するファイル、フックスクリプトである
config, domains.txt, hooks.sh の3つのファイルを作成している。
それぞれのファイルの内容は下記のようなものになる。

config:

```text
CONTACT_EMAIL='admin@example.com'
CHALLENGETYPE='dns-01'
KEYSIZE='2048'
```

config の内容では KEYSIZE は重要かもしれない。デフォルトだと4096になっており、
サービスによってはこのサイズの鍵を受け付けてくれない可能性もある。

domains.txt:

```text
example.com www.example.com
```

domains.txt は取得する証明書のコモンネームで、1行にスペース区切りで複数のドメインを記述しておけば
SAN に対応した証明書を作成することができる。
複数行に記載しておけば複数の証明書を作成できる。

hooks.sh:

```sh
#!/usr/bin/env bash

function deploy_challenge {
    local DOMAIN="${1}" TOKEN_FILENAME="${2}" TOKEN_VALUE="${3}"
    echo "Please create record sets following:"
    echo "_acme-challenge.${DOMAIN}. IN TXT \"${TOKEN_VALUE}\""
    read
}

function clean_challenge {
    local DOMAIN="${1}" TOKEN_FILENAME="${2}" TOKEN_VALUE="${3}"
}

function deploy_cert {
    local DOMAIN="${1}" KEYFILE="${2}" CERTFILE="${3}" FULLCHAINFILE="${4}" CHAINFILE="${5}" TIMESTAMP="${6}"
}

function unchanged_cert {
    local DOMAIN="${1}" KEYFILE="${2}" CERTFILE="${3}" FULLCHAINFILE="${4}" CHAINFILE="${5}"
}

HANDLER=$1; shift; $HANDLER $@
```

hooks.sh は dehydrated 実行時に連動して実行されるフックスクリプト。
実行権限を付けるのを忘れずに。

あとは以下のコマンドを実行。
```console
$ ./dehydrated --register --accept-terms -k ./hooks.sh
$ ./dehydrated --cron -k ./hooks.sh
```

deploy\_challenge 関数内の read で入力待ちになり、 表示されたレコードセットを設定してから Enter を押す。
うまく行けば certs ディレクトリ内に必要なファイル一式が作成されている。


