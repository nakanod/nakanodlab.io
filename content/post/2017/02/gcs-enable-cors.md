+++
title = "GCS で CORS を有効にする"
slug = "gcs-enable-cors"
tags = [
  "gcs",
]
date = "2017-02-12T18:43:09+09:00"
draft = false
markup = "md"

+++


gsutil コマンドを使う。あらかじめ json ファイル(ここでは cors.json )に設定を記述しておき、そのファイルと対象のバケットを指定する。
ここでは origin を `"*"` としているが、アクセス元がわかっているなら指定しておくほうが良い。

cors.json:

```json
[
  {
    "origin": [
      "*"
    ],
    "responseHeader": [
      "Content-Type"
    ],
    "method": [
      "GET",
      "HEAD",
      "OPTIONS"
    ],
    "maxAgeSeconds": 300
  }
]
```

設定

```console
$ gsutil cors set cors.json gs://bucket-name
```

確認するコマンドは以下

```console
$ gsutil cors get gs://bucket-name
```
