+++
draft = false
markup = "md"
title = "OpenSSL で PKCS#12 形式のファイルを作る"
slug = "openssl-pkcs12-creation"
tags = [
  "openssl",
]
date = "2017-02-16T20:41:34+09:00"

+++

Windows Server (IIS) の SSL 更新時の備忘録として。中間証明書がある場合は cat で1つにまとめる。

```console
$ cat cert.pem chain-cert.pem > bundled-cert.pem
```

次に以下のコマンドで、秘密鍵と証明書を1つにまとめる。
コマンド実行時にパスワードを決める。これはインポートする際に必要になるので忘れないように。
拡張子については .p12 でも .pfx でもどちらでもよさそう。
-name オプションにフレンドリ名を指定することもできる。

```console
$ openssl pkcs12 -export -in bundled-cert.pem -inkey key.pem -name 'friendly-name' -out bundled.pfx
```

以下は確認。作成されたファイルのうち秘密鍵を表示する。
3つのコマンドとも上記コマンドで決めたパスワードを入力しないと表示することはできない。

```console
$ openssl pkcs12 -in bundled.pfx -nocerts -nodes
```

証明書を表示。

```console
$ openssl pkcs12 -in bundled.pfx -clcerts -nokeys
```

中間証明書を表示。

```console
$ openssl pkcs12 -in bundled.pfx -cacerts -nokeys
```
