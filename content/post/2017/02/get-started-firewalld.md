+++
title = "firewalld 最初にやること"
slug = "get-started-firewalld"
tags = [
  "firewalld",
]
date = "2017-02-18T19:34:12+09:00"
draft = false
markup = "md"

+++

CentOS 7 でのオペレーションを想定。

まずはインストールと firewalld の起動、OSブート時の自動起動するようにする。

```console
# yum install firewalld
# systemctl start firewalld.service
# systemctl enable firewalld.service
```

デフォルトだと SSH がフルオープンなので、もし IP アドレスで制限をかけたいなら以下のようにする。
remove-service オプションで SSH 自体の許可を削除し、 add-rich-rule によって SSH に対しての詳細な許可を追加している。

```console
# firewall-cmd --parmanent --zone=public --remove-service=ssh
# firewall-cmd --parmanent --zone=public --add-rich-rule='rule family=ipv4 source address=192.0.2.1 service name=ssh accept'
# firewall-cmd --parmanent --zone=public --add-rich-rule='rule family=ipv4 source address=192.0.2.0/24 service name=ssh accept'
```

設定の反映

```console
# firewall-cmd --reload
```
