+++
slug = "python-get-file-lines-count"
title = "python で ファイルの行数を取得する"
tags = [
  "python",
]
date = "2017-02-05T20:43:28+09:00"
draft = false
markup = "md"

+++

wc -l 的なことをしたい。

```py
with open('/path/to/file', mode='r') as f:
    count = len(f.readlines())
```
