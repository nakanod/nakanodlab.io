+++
markup = "md"
slug = "ssh-no-host-key-check"
title = "初回 SSH 時のホストキーチェックをしない"
tags = [
  "openssh",
]
date = "2017-02-15T23:10:38+09:00"
draft = false

+++

一度も SSH していないサーバに対して SSH しようとすると yes/no の入力待ちが発生する。
たとえばバッチ処理などでサーバから別のサーバへ SSH したいときにはできればこれを回避したいので、そんな時どうするか。

~/.ssh/config などの ssh\_config に以下の2行を追記する。
セキュリティにリスクがあることを認識して設定すること。


```text
StrictHostKeyChecking no
UserKnownHostsFile /dev/null
```

