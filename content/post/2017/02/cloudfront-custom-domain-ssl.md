+++
date = "2017-02-07T23:06:29+09:00"
draft = false
markup = "md"
slug = "cloudfront-custom-domain-ssl"
title = "CloudFront で独自ドメインの SSL"
tags = [
  "aws", "cloudfront", "ssl",
]

+++

AWS のマネジメントコンソールでは、CloudFront あてに独自ドメインで SSL 通信させるために証明書をアップロードすることが現時点ではできなそう。
そのため、 awscli を使用して証明書をアップロードする。以下長いけど1つのコマンド。

```console
$ aws iam upload-server-certificate --server-certificate-name ssl.example.com --path /cloudfront/ --private-key file:///path/to/key.pem --certificate-body file:///path/to/cert.pem --certificate-chain file:///path/to/chain-cert.pem
```

--path オプションに `/cloudfront/` を指定するのがポイントの様子。
