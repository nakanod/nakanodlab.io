+++
date = "2017-11-12T17:53:51+09:00"
#updated = "2017-11-12T17:08:51+09:00"
draft = false
markup = "md"
title = "Pleroma を Docker で動かしてみる"
slug = "pleroma-on-docker"
toc = true
tags = ["docker", "pleroma"]
+++

# 前提

* Ubuntu 16.04
* Docker, Docker Compose はインストール済み
* Docker に必要なファイルとして Dockerfile, docker-compose.yml, .dockerignore の3つを新規作成する
* Nginx や PostgreSQL 等のセットアップについてはここでは関与しない

# 手順

## フォルダの作成

投稿時やアイコン変更などでアップロードしたファイルを Volume で外に出しておくためのフォルダを作成する

```console
$ sudo mkdir -p /var/lib/pleroma/uploads
```

## ソースコードをダウンロード

```console
$ git clone https://git.pleroma.social/pleroma/pleroma.git
$ cd pleroma/
```

## config/dev.secret.exs を新規作成

DB や URL の詳細な設定は適宜変更する

```elixir
use Mix.Config

config :pleroma, Pleroma.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "pleroma_dev",
  hostname: "localhost",
  pool_size: 10

config :pleroma, Pleroma.Web.Endpoint,
  url: [host: "pleroma.example.com", scheme: "https", port: 443]
```

## Docker に必要なファイルを新規作成

Dockerfile, docker-compose.yml, .dockerignore の3つを作成する

<script src="https://gist.github.com/nakanod/1b89d6c20588c8fa0903e2a1372bf7f0.js"></script>

## ビルド

```console
$ docker-compose build
```

## DBの作成とマイグレート

ホスト側に Elixir がインストールされていないので mix コマンドを実行したい場合は下記のようにする

```console
$ docker-compose run --rm app mix ecto.create
$ docker-compose run --rm app mix ecto.migrate
```

## 起動

```console
$ docker-compose up -d
```
