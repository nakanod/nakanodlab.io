+++
date = "2018-02-06T00:21:05+09:00"
#updated = "2018-02-05T22:21:05+09:00"
draft = false
markup = "md"
title = "nginx の ngx_brotli モジュールを有効にしてみる"
slug = "add-ngx_brotli-module-to-nginx"
toc = false
tags = ["gentoo"]
+++

brotli 圧縮をなんとなく試してみたくなったのでやってみた。

野良ビルドでのやり方であれば [ngx_brotli の README](https://github.com/google/ngx_brotli) を見ながらやるとよいが、
せっかく Gentoo を使っているので ebuild を修正して有効にできるようにしてみた。
そうすれば継続してパッケージマネージャで管理させることができる。

実際にこの修正を行う場合は公式の Portage ツリーを直に修正すると同期を取ったときに消されてしまうので、別に Portage ツリーを用意してそちらで行うのがよい。
修正に使用した ebuild ファイルは nginx-1.13.8.ebuild で、差分については大きく5箇所ある。

ソースの URL (Git リポジトリ) や作業ディレクトリを指定する。

```diff
--- a/nginx-1.13.8.ebuild 2018-02-02 13:02:11.866156348 +0000
+++ b/nginx-1.13.8.ebuild 2018-02-04 13:21:35.956572309 +0000
@@ -137,6 +137,10 @@
 HTTP_LDAP_MODULE_URI="https://github.com/kvspb/nginx-auth-ldap/archive/${HTTP_LDAP_MODULE_PV}.tar.gz"
 HTTP_LDAP_MODULE_WD="${WORKDIR}/nginx-auth-ldap-${HTTP_LDAP_MODULE_PV}"

+# nginx-brotli-module (https://github.com/google/ngx_brotli, BSD-2)
+HTTP_BROTLI_MODULE_URI="https://github.com/google/ngx_brotli.git"
+HTTP_BROTLI_MODULE_WD="${WORKDIR}/ngx_brotli"
+
 # We handle deps below ourselves
 SSL_DEPS_SKIP=1
 AUTOTOOLS_AUTO_DEPEND="no"
 
```

USE フラグを追加する。これで `nginx_modules_http_brotli` を指定できるようにする

```diff
@@ -207,7 +211,8 @@
        http_sticky
        http_mogilefs
        http_memc
-       http_auth_ldap"
+       http_auth_ldap
+       http_brotli"

 IUSE="aio debug +http +http2 +http-cache +ipv6 libatomic libressl luajit +pcre
        pcre-jit rtmp selinux ssl threads userland_GNU vim-syntax"
 
```

依存パッケージを追加する。
git clone, git submodule を実行するため git を追加している

```diff
@@ -282,7 +287,8 @@
                net-misc/curl
                www-servers/apache
        )
-       nginx_modules_http_auth_ldap? ( net-nds/openldap[ssl?] )"
+       nginx_modules_http_auth_ldap? ( net-nds/openldap[ssl?] )
+       nginx_modules_http_brotli? ( dev-vcs/git )"
 RDEPEND="${CDEPEND}
        selinux? ( sec-policy/selinux-nginx )
        !www-servers/nginx:0"
 
```

src_unpack 関数を追加する。関数最初の3行は src_unpack デフォルトの挙動で、
その後、このタイミングでしてよいのかはわからないが git clone, git submodule を実行する。

```diff
@@ -334,6 +340,20 @@
        fi
 }

+src_unpack() {
+       if [ "${A}" != "" ]; then
+               unpack ${A}
+       fi
+
+       if use nginx_modules_http_brotli; then
+               cd "${WORKDIR}" || die
+               git clone "${HTTP_BROTLI_MODULE_URI}" || die
+               cd "${WORKDIR}/ngx_brotli" || die
+               git submodule update --init || die
+               cd "${S}" || die
+       fi
+}
+
 src_prepare() {
        eapply "${FILESDIR}/${PN}-1.4.1-fix-perl-install-path.patch"
        eapply "${FILESDIR}/${PN}-httpoxy-mitigation-r1.patch"
 
```

src_configure 関数内で configure オプションを追記するようにする。

```diff
@@ -535,6 +555,11 @@
                myconf+=( --add-module=${HTTP_LDAP_MODULE_WD} )
        fi

+       if use nginx_modules_http_brotli; then
+               http_enabled=1
+               myconf+=( --add-module=${HTTP_BROTLI_MODULE_WD} )
+       fi
+
        if use http || use http-cache || use http2; then
                http_enabled=1
        fi
 
```

修正内容は以上。


この内容以外に、git clone ではなくソースをアーカイブからダウンロードする方法や、依存パッケージとして app-arch/brotli を追加してファイルにパッチをあてるなどの方法も試したが、
結局それらではビルドに失敗してしまい、このような形になった。もしかしたらうまくやる方法があるかもしれない。

そのほか、 ebuild のポリシーとしては src_unpack 関数で git clone 等を実行することについてどうなのかなども知りたい。

