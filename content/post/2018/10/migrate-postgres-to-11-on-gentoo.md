+++
date = "2018-10-21T16:39:13+09:00"
#updated = "2018-10-21T15:43:13+09:00"
draft = false
markup = "md"
title = "Gentoo Linux で PostgreSQL 10 を 11 へアップグレード"
slug = "migrate-postgres-to-11-on-gentoo"
toc = false
tags = ["postgresql", "gentoo"]
+++

## 概要

2018/10/18に PostgreSQL 11 がリリースされました。
PostgreSQL は Pleroma での DB サーバとして使用しており、
10.5 で稼働していたのでアップグレードすることにしました。

最初に移行に関してはひとことで言ってしまえば `pg_dumpall` で取得したダンプをインサートして終わりです。

環境として Gentoo Linux + systemd で stable 運用であることを前提としています。


## 手順

まずは accept keywords に PostgreSQL 11 を追加します。

/etc/portage/package.accept\_keywords/postgresql:
```text
dev-db/postgresql:11 ~amd64
```

Portage ツリーとパッケージの更新を行います。

```console
# emerge --sync --quiet
# emerge -uDN @world
```

PostgreSQL 11 がインストールされたら以下のコマンド実行します。初期設定が行われます。

```console
# emerge --config =dev-db/postgresql-11.0
```

現在の設定と差分を見て、差分があれば新環境用の設定ファイルを編集します。

```console
# diff -ubB /etc/postgresql-{10,11}/pg_hba.conf
# diff -ubB /etc/postgresql-{10,11}/pg_ident.conf
# diff -ubB /etc/postgresql-{10,11}/postgresql.conf

# vi /etc/postgresql-11/{pg_hba.conf,pg_ident.conf,postgresql.conf}
```

ここまで用意ができたら DB をダンプします。
整合性を保つため、DBへ書き込む可能性がある別プログラムがあれば停止しておくと良いでしょう。
(私の環境下では Pleroma がそれにあたり、あらかじめ停止しています。)
以下のコマンドで DB をダンプします。

```console
# pg_dumpall10 -U postgres -f /tmp/dump.sql
```

ダンプの取得が終わったら postgresql 10 を停止して 11 を起動します。

```console
# systemctl stop postgresql-10.service
# systemctl start postgresql-11.service
```

※余談ですが、本当は別ポートで10,11を並行で起動しておきたかったのですが、 postgresql.conf の port を変更してもうまくいきませんでした。
別ポートで起動するには systemd の unit file 側でポート番号を指定する箇所があるので、そちらを変更しなければなりませんでした。
これはあとから気づきました。

PostgreSQL 11 が起動したらダンプをインサートします。

```console
# psql11 -U postgres < /tmp/dump.sql
```

インサートが終われば移行は完了です。別プログラムを起動して正常動作するか確認しましょう。

最後に、順番として正しいかは不明ですが `eselect` でデフォルトのバージョンを10から11に変更します。

```console
# eselect postgresql set 11
```

以上です。
