+++
date = "2018-01-27T19:43:19+09:00"
#updated = "2018-01-27T00:43:19+09:00"
draft = false
markup = "md"
title = "systemd をアップグレードしたら再起動ができなくなってびびった"
slug = "upgrade-systemd-to-236-r5"
toc = false
tags = ["gentoo", "systemd"]
+++

## 概要

* systemd-236-r5 にアップグレードした
* USE フラグ sysv-utils が有効になった
* sysvinit パッケージが削除され reboot 等のシンボリックリンクが変更された
* reboot コマンドで再起動できず systemctl -f reboot でなら再起動できた
* 一度再起動してしまえば reboot も正常に動作した

## 顛末

Gentoo のシステム更新をおこなった際に systemd が 236-r5 にアップグレードされて、
その後、再起動しようと思い、 `reboot` コマンドを叩いたところシャットダウンプロセスに移行しない。
すぐにプロンプトが返ってきてしまい、コマンドが正しく実行されている様子がない。

```console
# reboot
# 
```

他にも `poweroff` や `init 6` やら試したものの、どれも同じでどうしたものかと思っていたら、
どうもアップグレードした際に USE フラグ sysv-utils が有効になったため、sysvinit パッケージが削除され、これらコマンドのシンボリックリンクが変更されていた。

```console
# ls -l /sbin/reboot
lrwxrwxrwx 1 root root 4 Jan 15 15:28 /sbin/reboot -> halt

# ls -l /sbin/reboot
lrwxrwxrwx 1 root root 16 Jan 26 14:35 /sbin/reboot -> ../bin/systemctl
```

その影響かどうか知らないがうまく動作していない感じだった。

systemd では `systemctl reboot` のようなコマンドで再起動できるようなので、それを試したが、エラーが出力され、シャットダウンしてくれない。

```console
# systemctl reboot
Failed to reboot system via logind: Unit reboot.target not found.
Failed to start reboot.target: Unit reboot.target not found.
```

もう少し見ると -f (--force) オプションがあるようなのでそれを付けて再度実行したら、これがうまくいき、ようやく再起動することができた。

```console
# systemctl -f reboot
Connection to 192.0.2.1 closed by remote host.
Connection to 192.0.2.1 closed.
```

再起動後は reboot コマンド等も正常に動作するようになって、めでたしめでたし。



## 余談

systemd-236-r5 ビルド時に PORTAGE\_TMPDIR (default: /var/tmp) が btrfs でマウントされていると失敗するということが起きた。
これは PORTAGE\_TMPDIR を /tmp (tmpfs) に変えてやるなどして回避することができた。

## 余談2

/etc/default/grub を以下のように修正して、 `grub-mkconfig` した。

```diff
-GRUB_CMDLINE_LINUX="init=/usr/lib/systemd/systemd .."
+GRUB_CMDLINE_LINUX="init=/lib/systemd/systemd .."
 
```

## 参考

* [systemd sysv\-utils blocker resolution – Gentoo Linux](https://www.gentoo.org/support/news-items/2018-01-23-systemd-blocker.html)
* [9\.4\. システムのシャットダウン、サスペンド、休止状態 \- Red Hat Customer Portal](https://access.redhat.com/documentation/ja-jp/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-power)
* [641262 – sys\-apps/systemd\-236 fails to install: No such file or directory: b'/var/tmp/portage/sys\-apps/systemd\-236/image/usr/share/man/man3/sd\_journal\_seek\_head\.3'](https://bugs.gentoo.org/641262)
* [systemd rootprefix migration – Gentoo Linux](https://www.gentoo.org/support/news-items/2017-07-16-systemd-rootprefix.html)
