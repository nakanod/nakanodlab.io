+++
date = "2018-01-07T14:14:12+09:00"
updated = "2018-01-07T21:18:12+09:00"
draft = false
markup = "md"
title = "Gentoo Linux を GCE 上で起動する"
slug = "gentoo-linux-on-gce"
toc = false
tags = ["gentoo", "gcp"]
+++

とりあえず起動するところまでできたので軽くメモ程度に残しておく。

# 大まかな手順

1. VirtualBox に Gentoo をインストールする。
2. ディスクを変換・圧縮して GCS へアップロードする。
3. アップロードしたファイルを元にカスタムイメージを作成する。
4. 作成したカスタムイメージから起動する。

## 1. VirtualBox に Gentoo をインストールする。

OS は64ビットを想定。
VirtualBox の初期設定として行ったこととしては

* ディスクの形式はVDIでサイズは10GBで作成
* 準仮想化インターフェイスをKVMに変更
* メモリサイズは2GB以上
* ネットワークアダプタータイプを準仮想化ネットワーク(virtio-net)に変更
* オーディオ・USB・シリアルポートは無効
* 光学ドライブに ISO 指定
    * Gentoo Minimal Installation CD ( https://www.gentoo.org/downloads/ ) のISOをダウンロードしておく

VirtualBox で Gentoo Minimal Installation CD を起動したあとは、 https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation を参考にインストールしていく。
おそらく Gentoo のインストールで最もネックになるであろうカーネルのコンフィグについては
GitHub Gist https://gist.github.com/nakanod/448041a2947816b057c0d2599871f145/0d9afa3824257fdc1978d09cde872e631036892c に上げた。
一応 Google の指定する要件( https://cloud.google.com/compute/docs/images/building-custom-os?hl=ja )を満たすコンフィグになっている。
ただしこれが最適なものであるかどうかは不明。

* ディスクパーティションの作成
    * MBR 形式、プライマリパーティションを2つ。sda1はサイズ200MBでブートフラグを付与。sda2のサイズは以降すべて
* ファイルシステムのフォーマット
    * sda1 は /boot 用で Ext2、 sda2 は / 用で Btrfs それぞれラベルも設定
* マウント
* stage3 のダウンロードと展開
* chroot の前準備
    * sys, dev, proc のマウントと resolv.conf のコピー
* chroot
* root のパスワード設定
* make.conf 編集
    * CFLAGS の修正( -march=haswell を追加)、 CXXFLAGS の追加
* repos.conf/gentoo.conf をコピーして作成
* portage tree のダウンロード
* 必要なパッケージ(grub, gentoo-sources, btrfs-progs)のインストール
* ブートローダ設定
    * grub-install 実行と /etc/fstab 編集
* カーネルのビルド
    * カーネルパッケージとバージョンは検証当時最新 stable の gentoo-sources-4.9.72
    * /usr/src/linux へ移動して前述した GitHub Gist を .config として作成
    * make menuconfig, make, make install, make modules\_install, grub-mkconfig 実行


### プロファイルの変更

時期的なものだったり、ダウンロードしたstage3がsystemd対応のものでなかったのもあるが、
プロファイルを default/linux/amd64/13.0 から default/linux/amd64/17.0/systemd に変更した。
変更する際の設定は https://wiki.gentoo.org/wiki/Systemd/ja や、 https://www.gentoo.org/support/news-items/2017-11-30-new-17-profiles.html を参考におこなった

* eselect コマンドでプロファイル変更
* GRUB オプション設定とコンフィグ再作成
    * /etc/default/grub を編集
    * `GRUB_CMDLINE_LINUX="init=/usr/lib/systemd/systemd console=ttyS0,38400n8d"`
    * grub-mkconfig 実行
* systemd-networkd 設定
    * /etc/systemd/network/50-dhcp.network 作成
    * systemd-networkd 起動設定
* systemd-resolved 設定
    * resolv.conf をシンボリックリンクとして再作成
    * systemd-resolved 起動設定
* libtool, gcc, binutils, glibc を再ビルド
* emerge -e @world


## 2. ディスクを変換・圧縮して GCS へアップロードする。(と、それ以降)

2-4までは https://cloud.google.com/compute/docs/images/import-existing-image?hl=ja のページを参考に行う

* VirtualBox の Gentoo ゲスト環境は停止しておく
* VBoxManage でディスク複製(フォーマットはRAW)
* 複製した RAW フォーマットのディスクを tar コマンドで圧縮
* GCS にバケット作成して tar.gz ファイルをアップロード
* アップロードしたファイルを元にカスタムイメージ作成
* カスタムイメージからインスタンス起動
