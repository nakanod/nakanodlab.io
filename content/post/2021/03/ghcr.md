+++
date = "2021-03-13T15:39:26+09:00"
#updated = "2021-03-13T15:39:26+09:00"
draft = false
markup = "md"
title = "GitHub Container Registry を試してみた"
slug = "ghcr"
toc = false
tags = ["ghcr", "docker"]
+++


certbot-dns-google を多少簡略に使用するために docker コンテナで動かすようにしてみて、 GitHub にコードを置いた。
では、コンテナイメージはどうするかってところで今までは Docker Hub を使用していたが、
せっかくなので GitHub Container Registry (ghcr.io) に置くようにしてみた。

当該のリポジトリとパッケージはこちら。

* [GitHub \- nakanod/docker\-certbot\-dns\-google](https://github.com/nakanod/docker-certbot-dns-google)
* [Package certbot\-dns\-google · GitHub](https://github.com/users/nakanod/packages/container/package/certbot-dns-google)


大まかな流れはGitHub のパーソナルアクセストークンを作成、 Feature Preview を有効化。その後、 ghcr.io へログイン、プッシュ

## パーソナルアクセストークンの作成

右上のアイコン -> Settings -> Developer settings -> Personal access token -> Generate new token とたどって作成画面へ遷移。
スコープには write:packages を選択。依存する権限も付与される。 作成されたトークンはあとで ghcr.io にログインする際に使用するのでメモる。
(私は一旦テキストファイル pat.txt という名前で保存した。)

## Feature Preview を有効化

右上のアイコン -> Feature Preview -> Improved container support を enable に変更。

## ghcr.io へのログイン

保存したトークンをパイプで渡してログイン

```sh
cat pat.txt | docker login ghcr.io --username nakanod --password-stdin
```

## プッシュ

イメージの指定は以下のようになる。

```text
ghcr.io/USERNAME/IMAGENAME
```

今回は docker-compose を使用していたので、 `docker-compose.yml` の `image` を以下のように指定

docker-compose.yml (抜粋):
```yaml
image: ghcr.io/nakanod/certbot-dns-google:1.13.0
```

プッシュ。
```sh
docker-compose push
```


## 参考

* [Creating a personal access token \- GitHub Docs](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token)
* [Enabling improved container support \- GitHub Docs](https://docs.github.com/en/packages/guides/enabling-improved-container-support)
* [Migrating to GitHub Container Registry for Docker images \- GitHub Docs](https://docs.github.com/en/packages/guides/migrating-to-github-container-registry-for-docker-images)
