+++
date = "2021-01-30T21:18:46+09:00"
#updated = "2021-01-30T21:18:46+09:00"
draft = false
markup = "md"
title = "Consul と Dnsmasq を試す"
slug = "consul-and-dnsmasq"
toc = true
tags = ["consul", "dnsmasq"]
+++

## 概要

サービスディスカバリやオーケストレーション周りについて全く触れてこなかったので、 Consul あたりを試してみた。
行っている内容はほぼ Consul 公式ドキュメントと同じなのでしっかり知りたいならそちらを見たほうが良い。 


2つのマシンにサーバーとクライアントを1つずつ用意して、
ついでに Dnsmasq で Consul DNS へ名前解決を転送するところまでを試してみた。


## 環境

* CentOS Stream 8
* Consul 1.9.2
* サーバー用インスタンス: 10.0.0.5
* クライアントインスタンス: 10.0.0.10

## 前準備

unzip や dig コマンドを使用するのでまずはそれらをインストール

```sh
sudo dnf install unzip bind-utils
```


## Consul のセットアップ


### インストール

```sh
curl https://releases.hashicorp.com/consul/1.9.2/consul_1.9.2_linux_amd64.zip -o consul_1.9.2_linux_amd64.zip
unzip consul_1.9.2_linux_amd64.zip
sudo mv consul /usr/local/bin
```

### 起動前準備

Consulのデータ用ディレクトリ、設定ファイル用ディレクトリとサービス設定ファイルを作成する。

```sh
mkdir ./consul
mkdir ./consul.d
vi ./consul.d/web.json
```
./consul.d/web.json:

```json
{
  "service": {
    "name": "web",
    "tags": [
      "rails"
    ],
    "port": 80
  }
}
```

### 起動

サーバー:

```sh
consul agent -server -bootstrap-expect=1 -node=agent-one -bind=10.0.0.5 -data-dir=./consul -config-dir=./consul.d
```

クライアント:

```sh
consul agent -node=agent-two -bind=10.0.0.10 -enable-script-checks=true -data-dir=./consul -config-dir=./consul.d
```

クライアントのみメンバー参加を実行する

```sh
consul join 10.0.0.5
```

## Dnsmasq のセットアップ

### インストール

```sh
sudo dnf install dnsmasq
```

### 設定ファイル

```sh
sudo vi /etc/dnsmasq.d/10-consul
```

/etc/dnsmasq.d/10-consul:

```text
server=/consul/127.0.0.1#8600
```

### 起動

```sh
sudo systemctl start dnsmasq.service
```

## 名前解決ができるか確認

```sh
dig @127.0.0.1 web.service.consul +short
```

問題なければインスタンス2台の IP アドレスが返ってくる。

## 参考サイト

* [Get Started \| Consul \- HashiCorp Learn](https://learn.hashicorp.com/collections/consul/getting-started)
* [Forward DNS for Consul Service Discovery \| Consul \- HashiCorp Learn](https://learn.hashicorp.com/tutorials/consul/dns-forwarding)
* [【Consul】dnsmasqで名前解決を行う方法を試してみた \| Pocketstudio\.jp log3](http://pocketstudio.jp/log3/2014/05/01/consul_with_dnsmasq_name_resolution/)

